FROM debian:stable-slim

COPY states /srv/salt/states
COPY saltstack.list /etc/apt/sources.list.d/saltstack.list
COPY saltstack.gpg /etc/apt/trusted.gpg.d/saltstack.gpg

RUN apt-get update && \
    apt-get install --yes --no-install-recommends salt-master salt-minion salt-api python-cherrypy3

COPY minion.bootstrap /etc/salt/minion

RUN salt-call --local --retcode-passthrough state.highstate

